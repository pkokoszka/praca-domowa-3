# -*- coding: utf-8 -*-

import unittest

from Program import *

class APItest(unittest.TestCase):
    def test_AdresStartowyIKoncowyOK(self):
        startAdd = 'Puszczyka 10, Łódź'
        endAdd = 'Gdańsk'
        try:
            tests(startAdd, endAdd)
        except:
            self.fail("Blad")


    def test_AdresStartowyZlyIKoncowyZly(self):
        startAdd = 'xxx'
        endAdd = 'xxx'
        try:
            StartMatchingRouteWithFourSquare(startAdd, endAdd)
        except:
            self.fail("Blad")

    def test_GeolokalizacjiOK(self):
        Add = 'Puszczyka 10, Łódź'
        try:
            lat, lng = GeolocalizeAddress(Add)
        except:
            self.fail("Blad")


    def test_GeolokalizacjiZLY(self):
            Add = 'xxxxx'
            try:
                lat, lng = GeolocalizeAddress(Add)
            except:
                self.fail("Blad")

if __name__ == '__main__':
    unittest.main()