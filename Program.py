# -*- coding: utf-8 -*-
import pygmaps
from pprint import pprint
import sys
from models.DrivePoint import *
from models.FourSquarePoint import *
import requests
import webbrowser
import os.path

RouteWaypoints = []
#FourSquareWaypoints = []


def GeolocalizeAddress(address):
    google_address = 'https://maps.googleapis.com/maps/api/geocode/json'
    google_params = {'address': address, 'sensor': 'false'}
    google_json = requests.get(google_address, params=google_params).json()

    if not google_json['results']:
        print 'Nie znaleziono'
        sys.exit()

    lat = google_json['results'][0]['geometry']['location']['lat']
    lng = google_json['results'][0]['geometry']['location']['lng']
    return lat, lng




def StartMatchingRouteWithFourSquare(startAddress, endAddress):
    google_address = 'https://maps.googleapis.com/maps/api/directions/json'
    google_params = {'origin': startAddress, 'destination': endAddress, 'sensor': 'false', 'key':"AIzaSyAzrPzeYMi6Ht5A71hfdHTqeTcGJze5-jM"}
    google_json = requests.get(google_address, params=google_params).json()

    if not google_json['routes']:
        print 'Nie znaleziono - wystąpił błąd'
        sys.exit()

    #pprint(google_json['routes'])
    drivePoints = google_json['routes'][0]['legs'][0]['steps']
    for point in drivePoints:
        pp = make_drivePoint(point['start_location']['lat'], point['start_location']['lng'])
        RouteWaypoints.append(pp)
        #print(str(pp.lat) + "|" + str(pp.lng))



    foursquare_address = 'https://api.foursquare.com/v2/venues/search'
    for waypoint in RouteWaypoints:
        foursquare_params = {'ll': str(waypoint.lat) + "," + str(waypoint.lng),'categoryId':'4bf58dd8d48988d11b941735,4bf58dd8d48988d1ca941735,5283c7b4e4b094cb91ec88d7,4bf58dd8d48988d16e941735','radius':'1000','intent':'browse','v': '20140323', 'oauth_token': "0VHFINYT1ZRQM413KOIWYQUE51RUNMNFP5VXWDQ0JKSGYMQG"}
        foursquare_json = requests.get(foursquare_address, params=foursquare_params).json()

        #Address = make_FourSquareAddress()
        #pprint(foursquare_json['response']['suggestedRadius'])

        if not foursquare_json['response']:
            print 'Nie znaleziono - wystąpił błąd - API zostało przeciążone, odczekaj parę minut'
            sys.exit()

        for point in foursquare_json['response']['venues']:
            #print(point['name'])
            try:
                name = str(point['name'])
            except:
                name = '""'
            pp = make_FourSquarePoint(point['location']['lat'], point['location']['lng'], name.replace('"', ""), point['categories'][0]['name'], point['id']) #tymczasowe rozwiązanie
            waypoint.listOfPoints.append(pp)
            #print(pp.name)
            #print"#######################################"




    newPath = []
    for point in RouteWaypoints:
        pp = [point.lat, point.lng]
        newPath.append(pp)

    lat, lng = GeolocalizeAddress(startAddress)

    map = pygmaps.maps(lat, lng, 8)
    map.addpath(newPath, "#00FF00")

    for routeWaypoint in RouteWaypoints:
        map.addradpoint(routeWaypoint.lat, routeWaypoint.lng, 1000, "#FF0000")
        for fourSquarePoint in routeWaypoint.listOfPoints:
            nameToAdd = fourSquarePoint.name

            # nameToAdd = nameToAdd.encode('utf-8').replace("",)
            # if "Aїoli" in nameToAdd:
            #     try:
            #         nameToAdd = nameToAdd.replace('ї', 'i')
            #     except:
            #         nameToAdd = "blad nazwy"

            map.addpoint(fourSquarePoint.lat, fourSquarePoint.lng, "#00FFFF", nameToAdd) #


    map.draw('./mymap.html')


    print("Zadanie zakończone - odpalam wygenerowaną stronę.")
    webbrowser.open("file:///" + os.path.abspath('mymap.html'))

def tests(startAddrr, endAddrr):

    StartMatchingRouteWithFourSquare(startAddrr,endAddrr)


if __name__ == '__main__':
    startAddress = raw_input('Podaj adres początkowy (np. Warszawa, Nowoursynowska 11): ')
    endAddress = raw_input('Podaj adres końcowy (np. Poznań, Łazarz): ')
    StartMatchingRouteWithFourSquare(startAddress,endAddress)