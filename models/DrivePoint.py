# -*- coding: utf-8 -*-

class DrivePoint(object):
    lat = ""
    lng = ""
    listOfPoints = []


    def __init__(self, lat, lng):
        self.lat = lat
        self.lng = lng

def make_drivePoint(lat, lng):
    drivePoint = DrivePoint(lat, lng)
    return drivePoint
