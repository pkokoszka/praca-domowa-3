# -*- coding: utf-8 -*-


class FourSquarePoint(object):
    lat = 0
    lng = 0
    name = ""
    id = ""
    categoryId = ""


    def __init__(self, lat, lng, name, categoryId, id):
        self.lat = lat
        self.lng = lng
        self.categoryId = categoryId
        self.name = name
        self.id = id

def make_FourSquarePoint(lat, lng, name, categoryId, id):
    fourSquarePoint = FourSquarePoint(lat, lng, name, categoryId, id)
    return fourSquarePoint
